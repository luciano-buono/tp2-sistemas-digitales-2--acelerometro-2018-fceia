/*
 * display.h
 *
 *  Created on: 6/5/2018
 *      Author: Lucho
 */

#ifndef INCLUDES_DISPLAY_H_
#define INCLUDES_DISPLAY_H_

#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>


// SDK Included Files
#include "fsl_sim_hal.h"
#include "fsl_lpsci_hal.h"
#include "fsl_clock_manager.h"
#include "fsl_pit_hal.h"
//#include "fsl_slcd_hal.h"

// Project Included Files
#include "board.h"
#include "key.h"
#include "LCD.h"





#endif /* INCLUDES_DISPLAY_H_ */
