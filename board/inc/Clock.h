/*
 * Clock.h
 *
 *  Created on: 7/5/2018
 *      Author: Lucho
 */

#ifndef INCLUDES_CLOCK_H_
#define INCLUDES_CLOCK_H_



#include "fsl_device_registers.h"
#include "board.h"
#include "fsl_clock_manager.h"

#include "fsl_sim_hal.h"
#include "fsl_mcg_hal.h"
#include "fsl_tpm_hal.h"
#include "fsl_smc_hal.h"
#include "MKL46Z4.h"
#include "fsl_port_hal.h"

/*==================[macros and definitions]=================================*/

/* OSC0 configuration. */
#define OSC0_XTAL_FREQ 8000000U

/* EXTAL0 PTA18 */
#define EXTAL0_PORT   PORTA
#define EXTAL0_PIN    18
#define EXTAL0_PINMUX kPortPinDisabled

/* XTAL0 PTA19 */
#define XTAL0_PORT   PORTA
#define XTAL0_PIN    19
#define XTAL0_PINMUX kPortPinDisabled

/*==================[internal data declaration]==============================*/

/* Configuration for enter VLPR mode. Core clock = 4MHz. */
const clock_manager_user_config_t clockConfigVlpr =
{
    .mcgConfig =
    {
        .mcg_mode           = kMcgModeBLPI, // Work in BLPI mode.
        .irclkEnable        = true,  		// MCGIRCLK enable.
        .irclkEnableInStop  = false, 		// MCGIRCLK disable in STOP mode.
        .ircs               = kMcgIrcFast, 	// Select IRC4M.
		.fcrdiv             = 0,    		// FCRDIV 0. Divide Factor is 1

        .frdiv   = 0,						// No afecta en este modo
        .drs     = kMcgDcoRangeSelLow,  	// No afecta en este modo
        .dmx32   = kMcgDmx32Default,    	// No afecta en este modo

        .pll0EnableInFllMode = false,  		// No afecta en este modo
        .pll0EnableInStop  = false,  		// No afecta en este modo
        .prdiv0            = 0b00,			// No afecta en este modo
        .vdiv0             = 0b00,			// No afecta en este modo
    },
    .simConfig =
    {
        .pllFllSel = kClockPllFllSelPll,	// No afecta en este modo
        .er32kSrc  = kClockEr32kSrcLpo,     // ERCLK32K selection, use LPO.
        .outdiv1   = 0b0000,				// tener cuidado con frecuencias máximas de este modo
        .outdiv4   = 0b101,					// tener cuidado con frecuencias máximas de este modo
    },
    .oscerConfig =
    {
        .enable       = true,	  			// OSCERCLK enable.
        .enableInStop = true, 				// OSCERCLK enable in STOP mode.
    }
};

/* Configuration for enter RUN mode. Core clock = 48MHz. */
const clock_manager_user_config_t clockConfigRun =
{
    .mcgConfig =
    {
        .mcg_mode           = kMcgModePEE,  // Work in PEE mode.
        .irclkEnable        = true,  		// MCGIRCLK enable.
        .irclkEnableInStop  = false, 		// MCGIRCLK disable in STOP mode.
        .ircs               = kMcgIrcFast, 	// Select IRC4M.
        .fcrdiv             = 0,    		// FCRDIV 0. Divide Factor is 1

        .frdiv   = 4,						// Divide Factor is 4 (128) (de todas maneras no afecta porque no usamos FLL)
        .drs     = kMcgDcoRangeSelMid,  	// mid frequency range (idem anterior)
        .dmx32   = kMcgDmx32Default,    	// DCO has a default range of 25% (idem anterior)

        .pll0EnableInFllMode = true,  		// PLL0 enable in FLL mode
        .pll0EnableInStop  = true,  		// PLL0 enable in STOP mode
        .prdiv0            = 0b11,			// divide factor 4 (Cristal 8Mhz / 4 * 24)
        .vdiv0             = 0b00,			// multiply factor 24
    },
    .simConfig =
    {
        .pllFllSel = kClockPllFllSelPll,    // PLLFLLSEL select PLL.
        .er32kSrc  = kClockEr32kSrcLpo,     // ERCLK32K selection, use LPO.
        .outdiv1   = 0b0000,				// Divide-by-1.
        .outdiv4   = 0b001,					// Divide-by-2.
    },
    .oscerConfig =
    {
        .enable       = true,  				// OSCERCLK enable.
        .enableInStop = true, 				// OSCERCLK enable in STOP mode.
    }
};

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

void TPM_Init(void);
void InitOsc0(void);
/* Initialize clock. */
void ClockInit(void);
/*==================[external functions definition]==========================*/




///////////////////////////////////////////////////////////////////////////////
// TPM0 IRQ
///////////////////////////////////////////////////////////////////////////////

void TPM0_IRQHandler(void);

/*==================[end of file]============================================*/















#endif /* INCLUDES_CLOCK_H_ */
