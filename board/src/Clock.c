#include "Clock.h"

#include "fsl_device_registers.h"
#include "board.h"
#include "fsl_clock_manager.h"

#include "fsl_sim_hal.h"
#include "fsl_mcg_hal.h"
#include "fsl_tpm_hal.h"
#include "fsl_smc_hal.h"
#include "MKL46Z4.h"
#include "fsl_port_hal.h"

/*==================[macros and definitions]=================================*/



/*==================[internal data declaration]==============================*/



/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

void TPM_Init(void)
{
	uint32_t channel = 0;

	// Habilito clock del periferico
	SIM_HAL_EnableClock(SIM, kSimClockGateTpm0);

	// Detengo cuenta del Timer
	TPM_HAL_SetClockMode(TPM0, kTpmClockSourceNoneClk);

	// Reseteo el timer
	TPM_HAL_Reset(TPM0, 0);

	// Clock prescaler = 128
	TPM_HAL_SetClockDiv(TPM0, kTpmDividedBy128);

	// Borro bandera de overflow
	TPM_HAL_ClearTimerOverflowFlag(TPM0);

	// Configuro valor MOD a TPM_MOD_MOD_MASK (0xFFFF)
	TPM_HAL_SetMod(TPM0, TPM_MOD_MOD_MASK);

	// Uso TPM solo como timer -- Deshabilito todos los canales
	for (channel = 0; channel < FSL_FEATURE_TPM_CHANNEL_COUNT ; channel++)
	{
		TPM_HAL_DisableChn(TPM0, channel);
	}

	// Cuenta ascendente
	TPM_HAL_SetCpwms(TPM0, 0);

	// Activo interupcion ante bandera de OverFlow
	TPM_HAL_EnableTimerOverflowInt(TPM0);

	// Elijo la fuente de clock para el TPM (OSCERCLK)
	CLOCK_HAL_SetTpmSrc(SIM, 0, kClockTpmSrcOsc0erClk);

	// Habilito el Timer -- Fuente de clock interna
	TPM_HAL_SetClockMode(TPM0, kTpmClockSourceModuleClk);
}

void InitOsc0(void)
{
    // OSC0 configuration.
    osc_user_config_t osc0Config =
    {
        .freq                = OSC0_XTAL_FREQ,
        .hgo                 = kOscGainLow,
        .range               = kOscRangeVeryHigh,
        .erefs               = kOscSrcOsc,
        .enableCapacitor2p   = false,
        .enableCapacitor4p   = false,
        .enableCapacitor8p   = false,
        .enableCapacitor16p  = false,
    };

    /* Setup board clock source. */
    // Setup OSC0 if used.
    // Configure OSC0 pin mux.
    PORT_HAL_SetMuxMode(EXTAL0_PORT, EXTAL0_PIN, EXTAL0_PINMUX);
    PORT_HAL_SetMuxMode(XTAL0_PORT, XTAL0_PIN, XTAL0_PINMUX);

    CLOCK_SYS_OscInit(0U, &osc0Config);
}

/* Initialize clock. */
void ClockInit(void)
{
    /* Set allowed power mode, allow all. */
    SMC_HAL_SetProtection(SMC, kAllowPowerModeAll);

    InitOsc0();

    CLOCK_SYS_SetConfiguration(&clockConfigRun);
}

/*==================[external functions definition]==========================*/




///////////////////////////////////////////////////////////////////////////////
// TPM0 IRQ
///////////////////////////////////////////////////////////////////////////////

void TPM0_IRQHandler(void)
{
	// Borro bandera de overflow
	TPM_HAL_ClearTimerOverflowFlag(TPM0);

	board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_TOGGLE);
}

/*==================[end of file]============================================*/












