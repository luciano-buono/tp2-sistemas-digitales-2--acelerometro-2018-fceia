/* Copyright 2017, DSI FCEIA UNR - Sistemas Digitales 2
 *    DSI: http://www.dsi.fceia.unr.edu.ar/
 * Copyright 2017, Diego Alegrechi
 * Copyright 2017, Gustavo Muro
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/


#include "fsl_device_registers.h"
#include "board.h"
#include "fsl_clock_manager.h"

#include "fsl_sim_hal.h"
#include "fsl_mcg_hal.h"
#include "fsl_tpm_hal.h"
#include "fsl_smc_hal.h"
#include "MKL46Z4.h"
#include "fsl_port_hal.h"
// SDK Included Files

#include "fsl_lpsci_hal.h"
#include "fsl_clock_manager.h"
#include "fsl_pit_hal.h"
//#include "fsl_slcd_hal.h"
/*==================[macros and definitions]=================================*/



/*==================[internal data declaration]==============================*/





/////AAAAAAAAAAAAAAAAAAAAAAAA

#include "mma8451.h"

#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <Clock.h>



// Project Included Files
#include "board.h"
#include "key.h"
#include "LCD.h"
#include "Clock.h"


/*==================[macros and definitions]=================================*/


/*==================[internal data declaration]==============================*/
typedef enum
{
	STBY = 0,
	CONV,
	SHOW,

}estado_enum;

uint8_t temp8;

int Parpadeo_Rojo=0;
uint16_t Lecturamaxima,Conversiones_En_reposo, Lectura_actual;

estado_enum estado=STBY;
int cont10s=0,reposo=0;

/*==================[internal functions declaration]=========================*/
void PIT_Init(void);
void mma8451_FREEFALL_init(void);
 uint8_t mma8451_read_reg(uint8_t addr);
 void Leer_DRDY(void);
 void Mostrar_LCD(uint16_t dato);



/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/



/*==================[external functions definition]==========================*/

int main(void)
{	int16_t aux=0;
//	CLOCK_SYS_SetConfiguration(&clockConfigRun);
	board_init(); // Se inicializan funciones de la placa
	PIT_Init();   // INT CADA 0.5MS

	ClockInit();

	// Inicializa keyboard
	key_init();

	mma8451_init_FF_DRDY();
	//Comenzamos con clock en VLPR
	//CLOCK_SYS_SetConfiguration(&clockConfigVlpr);

	estado=STBY;
	while (1)
	{

		switch(estado){
			case STBY:
				Parpadeo_Rojo=0;
				mma8451_setLectMaxTo0();
				board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_OFF);
				board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_OFF);

				aux= mma8451_get_IntFlag_FF(); //Interrupt Flag
				if( aux ){  //comprobar que en modo FF
					//	CLOCK_SYS_SetConfiguration(&clockConfigRun);
						Parpadeo_Rojo=1;
						mma8451_2DRDY();  //ahora accel en modo DRDY

						estado=CONV;
					}
				break;




			case CONV:
				Parpadeo_Rojo=1;
				//
				Lectura_actual = sqrt(  mma8451_getAcCuad() ) ;
				Mostrar_LCD( sqrt(  mma8451_getAcCuad()  )  ) ;
				//

				if( sqrt (mma8451_getAcCuad() ) >=95 && sqrt( mma8451_getAcCuad() )<=105 )
					Conversiones_En_reposo ++;

				if(Conversiones_En_reposo > 10000){  //Timer de x segundos para que convierta y encuentre el max
					estado=SHOW;
					Lecturamaxima = sqrt(  mma8451_getLectMaxCuad()   );
					Mostrar_LCD( sqrt(  mma8451_getLectMaxCuad()  )  ) ;
					Conversiones_En_reposo=0;}
				break;

			case SHOW:
				board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_ON);
				if(cont10s >= 10000  ||   key_getPressEv(BOARD_SW_ID_1)){//10s
							cont10s=0;
							estado=STBY;
							mma8451_2FF();
						//	CLOCK_SYS_SetConfiguration(&clockConfigVlpr);
						}


				break;

			default:
					estado=STBY;

		}

    }

}





void PIT_IRQHandler(void)
{
	static int cont_led_toggle=0,cont3s=0;

    PIT_HAL_ClearIntFlag(PIT, 1);

    key_periodicTask1ms();
    if(Parpadeo_Rojo){  //SI esta ON LED Parpadea
    	cont_led_toggle++;
    	if(cont_led_toggle >= 1000){
    		board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_TOGGLE);
    		cont_led_toggle=0;
    	}
    }

    if(estado == SHOW){

    	cont10s++;

    }

    if(estado == CONV){
    	cont3s++;
    	if(cont3s >= 15000){
    		cont3s=0;
    		reposo=1;
    }
    }

}




void Mostrar_LCD(uint16_t dato){
	uint8_t BufLCD[6];
	sprintf((char *)BufLCD,"%4d",dato);
	vfnLCD_Write_Msg(BufLCD);
}



void PIT_Init(void)
{
    uint32_t frecPerif;

    SIM_HAL_EnableClock(SIM, kSimClockGatePit0);
    frecPerif=0;
    PIT_HAL_Enable(PIT);
    CLOCK_SYS_GetFreq(kBusClock, &frecPerif);
    PIT_HAL_SetTimerPeriodByCount(PIT, 1, frecPerif/1000);
    PIT_HAL_SetIntCmd(PIT, 1, true);
    PIT_HAL_SetTimerRunInDebugCmd(PIT, false);
    PIT_HAL_StartTimer(PIT, 1);
    NVIC_ClearPendingIRQ(PIT_IRQn);
    NVIC_EnableIRQ(PIT_IRQn);
}





/*==================[end of file]============================================*/













